﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Runtime.Serialization.Json;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ViewFeatures;
using Microsoft.Extensions.FileProviders;
using MVPlayer.Models;
using Newtonsoft.Json;

namespace MVPlayer.Controllers
{
    public class HomeController : Controller
    {
        private IHostingEnvironment _env;
        private static readonly HttpClient client = new HttpClient();
        private IHttpContextAccessor _accessor;
        private readonly PlayerContext _context;
        private static List<AssetItem> currentAseets = new List<AssetItem> { };

        public HomeController(IHostingEnvironment env, IHttpContextAccessor accessor, PlayerContext context)
        {
            _env = env;
            _accessor = accessor;
            _context = context;

        }

        public async Task<IActionResult> Index()
        {
            var webRoot = _env.WebRootPath;
            var tokenFile = System.IO.Path.Combine(webRoot, "data","token.json");
            var pairFile = System.IO.Path.Combine(webRoot, "data", "paired.json");

            if (!System.IO.File.Exists(tokenFile) || !System.IO.File.Exists(pairFile)) {
                return RedirectToAction("Pair");
            }

            int assetCount = await SyncAssets();

            if (assetCount == 0) {
                return RedirectToAction("NoData");
            }

            return RedirectToAction("Player");
        }

        public IActionResult Paired() {

            var webRoot = _env.WebRootPath;
            var tokenFile = System.IO.Path.Combine(webRoot, "data", "paired.json");
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(
                new MediaTypeWithQualityHeaderValue("application/json"));

           
            TextWriter writer;

            using (writer = new StreamWriter(tokenFile, append: false))
            {
                writer.WriteLine("paired");
            }

            return Content("");
        }

        public IActionResult Player() {

            var webRoot = _env.WebRootPath;
            var tokenFile = System.IO.Path.Combine(webRoot, "data", "token.json");

            PairResponse token = JsonConvert.DeserializeObject<PairResponse>(System.IO.File.ReadAllText(tokenFile));

            ViewBag.Token = token.Token;


            return View();
        }


        public IActionResult NoData() {

            var webRoot = _env.WebRootPath;
            var tokenFile = System.IO.Path.Combine(webRoot, "data", "token.json");

            PairResponse token = JsonConvert.DeserializeObject<PairResponse>(System.IO.File.ReadAllText(tokenFile));

            ViewBag.Token = token.Token;

            return View();
        }
        public async Task<IActionResult> ReloadData()
        {
           int count = await SyncAssets();

            return Json(new{ assetCount = count });
        }

        public async Task<int> SyncAssets() {



            var webRoot = _env.WebRootPath;
            var tokenFile = System.IO.Path.Combine(webRoot, "data", "token.json");

            PairResponse token = JsonConvert.DeserializeObject<PairResponse>(System.IO.File.ReadAllText(tokenFile));

            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(
                new MediaTypeWithQualityHeaderValue("application/json"));
            if (client.DefaultRequestHeaders.Contains("Token")) {
                client.DefaultRequestHeaders.Remove("Token");
            }
            client.DefaultRequestHeaders.Add("Token", token.Token);


            var assetTask = await client.GetAsync("https://mallventures.cz/api/assets");
          
            var assets = new List<Asset> { };
            if (assetTask.IsSuccessStatusCode)
            {
                string returnValue = await assetTask.Content.ReadAsStringAsync();

                assets = JsonConvert.DeserializeObject<List<Asset>>(returnValue);
            }

            int assetCount = assets.Count();

            if (assetCount > 0) {

                if (_context.AssetItems.Count() > 0)
                {
                    var currentKeys = assets.Select(a => a.id).ToList();

                    var assetsToDelete = _context.AssetItems.Where(a => !currentKeys.Contains(a.Key)).ToList();

                    if (assetsToDelete.Count() > 0) {
                        _context.AssetItems.RemoveRange(assetsToDelete);
                        await _context.SaveChangesAsync();
                    }

                    foreach (var asset in assets)
                    {
                        if (AssetItemExists(asset.id))
                        {
                            var existingAssetItem = _context.AssetItems.FirstOrDefault(a => a.Key == asset.id);
                            existingAssetItem.Duration = asset.duration;
                                existingAssetItem.EndDate = asset.endDate;
                                existingAssetItem.StartDate = asset.startDate;
                                existingAssetItem.Source = asset.source;
                                existingAssetItem.Key = asset.id;
                                existingAssetItem.Type = asset.type;
                                existingAssetItem.MimeType = asset.mimeType;
                                existingAssetItem.SortOrder = asset.sortOrder;
                                existingAssetItem.WeekDays = asset.weekDays;
                            _context.Entry(existingAssetItem).State = Microsoft.EntityFrameworkCore.EntityState.Modified;
                        }
                        else {

                            var assetItem = new AssetItem
                            {
                                Duration = asset.duration,
                                EndDate = asset.endDate,
                                StartDate = asset.startDate,
                                Source = asset.source,
                                Key = asset.id,
                                Type = asset.type,
                                MimeType = asset.mimeType,
                                SortOrder = asset.sortOrder,
                                WeekDays = asset.weekDays
                            };

                            _context.AssetItems.Add(assetItem);

                        }


                    }
                    await _context.SaveChangesAsync();

                }
                else {

                    foreach (var asset in assets)
                    {
                        var assetItem = new AssetItem
                        {
                            Duration = asset.duration,
                            EndDate = asset.endDate,
                            StartDate = asset.startDate,
                            Source = asset.source,
                            Key = asset.id,
                            Type = asset.type,
                            MimeType = asset.mimeType,
                            SortOrder = asset.sortOrder,
                            WeekDays = asset.weekDays
                        };

                        _context.AssetItems.Add(assetItem);


                    }

                    await _context.SaveChangesAsync();

                }

               

            }

            if (assetCount > 0) {
              bool downloadOK =  DownloadFiles();

                if (!downloadOK) {
                    var allCurrentAssets = _context.AssetItems.ToList();
                    _context.RemoveRange(allCurrentAssets);
                    await _context.SaveChangesAsync();
                    assetCount = 0;
                }

            }
            if (assetCount > 0) {
                currentAseets = _context.AssetItems.ToList();
            }

            return assetCount;
        }

        public bool DownloadFiles() {
            var webRoot = _env.WebRootPath;

            try
            {

                var assets = _context.AssetItems.ToList();

                foreach(var asset in assets) {

                    string url = "";
                    if (asset.Type == 0)
                    {
                        

                        url = "https://mallventures.cz/cloud/assets/" + asset.Source;
                    }
                    else {
                        url = "https://mallventuresstorage.blob.core.windows.net/assets/" + asset.Source;
                    }

                    Uri ur = new Uri(url);

                    string destination = asset.Source;

                    if (destination.Contains("?"))
                    {
                        string[] sources = destination.Split('?');
                        destination = sources[0];
                    }

                    var fileName = System.IO.Path.Combine(webRoot, "data/media", destination) ;
                    WebClient Client = new WebClient();
                    Client.DownloadFile(ur, fileName);

                    asset.IsLocal = true;
                    _context.Entry(asset).State = Microsoft.EntityFrameworkCore.EntityState.Modified;
                    _context.SaveChanges();

                }

                return true;
            }
            catch (Exception ex) {
                Console.WriteLine(ex.Message);
                return false;
            }

        }

        private bool AssetItemExists(int id)
        {
            return _context.AssetItems.Any(e => e.Key == id);
        }

        public IActionResult NextAsset(int id) {
            var allAssets = currentAseets;

            int todayNumber = (int)DateTime.Now.DayOfWeek;
            DateTime today = DateTime.Now;

            var assets = allAssets.Where(a => a.WeekDays.Contains(todayNumber.ToString()) && a.StartDate < today && a.EndDate > today).OrderBy(a => a.SortOrder).ToList();
            var lastAssetId = assets.Last().Id;

            var prevAsset = assets.Where(a => a.Id == id).FirstOrDefault();

            if (id == 0 || lastAssetId == id || prevAsset == null) {
                return Json(assets.First());
            }

            int prevIndex = assets.IndexOf(prevAsset);
            int nextIndex = prevIndex + 1;
            var nextAsset = assets.ElementAt(nextIndex);

            return Json(nextAsset);
        }

        public async Task<IActionResult> Pair()
        {
            var webRoot = _env.WebRootPath;
            var tokenFile = System.IO.Path.Combine(webRoot, "data", "token.json");
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(
                new MediaTypeWithQualityHeaderValue("application/json"));

            PairRequest req = new PairRequest {
                IpAddress = _accessor.HttpContext.Connection.RemoteIpAddress.ToString(),
                MacAddress = "54"
            };

            var postTask = await client.PostAsJsonAsync("https://mallventures.cz/api/players", req);
            string returnValue = await postTask.Content.ReadAsStringAsync();

            TextWriter writer;

            using (writer = new StreamWriter(tokenFile, append: false))
            {
                writer.WriteLine(returnValue);
            }

            PairResponse token = JsonConvert.DeserializeObject<PairResponse>(System.IO.File.ReadAllText(tokenFile));

            return View(token);
        }


    


        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
