﻿var connection = $.hubConnection('https://mallventures.cz', { qs: "token=" + token });

var mallHubProxy = connection.createHubProxy('mallHub');
mallHubProxy.on('receiveMessageFromCloud', function (message) {
    console.log(message);

    if (message == 'data-change') {
        $.get("/home/ReloadData", function (data) {
            if (data.assetCount > 0) {
                window.location.href = "/home/index";
            }
        });
    }

});


connection.start()
    .done(function () { console.log('Now connected, connection ID=' + connection.id); })
    .fail(function () { console.log('Could not connect'); });