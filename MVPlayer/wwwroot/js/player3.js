﻿var currentAsset;
var currentIndex = 0;
var currentTimeout;
var connection;
var mallHubProxy;

$(document).ready(function () {

    connection = $.hubConnection('https://mallventures.cz', { qs: "token=" + token });

    mallHubProxy = connection.createHubProxy('mallHub');
    mallHubProxy.on('receiveMessageFromCloud', function (message) {
        if (message == 'data-change') {

            window.clearTimeout(currentTimeout);
            currentIndex = 0;
            $.get("/home/ReloadData", function (data) {
                if (data.assetCount > 0) {
                    currentTimeout = window.setTimeout(getNextAsset, 10000)
                }
                else {
                    window.location.href = "/home/index";
                }
            });

        }

    });


    connection.start()
        .done(function () {
            console.log('Now connected, connection ID=' + connection.id);
            getNextAsset();
        })
        .fail(function () {
            console.log('Could not connect');

        });

});

function getNextAsset() {

    //var queryStrinParameter = getParameterByName("wantedasset");

    //if (queryStrinParameter != null && queryStrinParameter != '') {
    //    currentIndex = parseInt(queryStrinParameter);
    //}
   

    $.getJSON('/home/nextasset?id=' + currentIndex, function (data) {
        currentAsset = data;
        currentIndex = currentAsset.id;
        buildPlayer();
    });

}

function buildPlayer() {

    var htmlData = "";

    var source = currentAsset.source;
    var type = currentAsset.type;
    var duration = currentAsset.duration;
    var mime = currentAsset.mimeType;
    var local = currentAsset.isLocal;

    window.clearTimeout(currentTimeout);

    mallHubProxy.invoke('reportViewToCloud', token, currentAsset.key).done(function () {
   //     console.log('Invocation of reportViewToCloud succeeded');
    }).fail(function (error) {
    //    console.log('Invocation of reportViewToCloud failed. Error: ' + error);
    });

    if (type == 0) {
        if (local) {
            htmlData += '<img class="player-img" src="/data/media/' + source + '" />';
        }
        else {
            htmlData += '<img class="player-img" src="https://mallventures.cz/cloud/assets/' + source + '" />';
        }
    }
    else {
        htmlData += '<video muted autoplay>';
        if (local) {
            htmlData += '<source src="/data/media/' + source + '" type="' + mime + '">';
        }
        else {
            htmlData += '<source src="https://mallventuresstorage.blob.core.windows.net/assets/' + source + '" type="' + mime + '">';
        }
        htmlData += '</video>';
    }

    $('#player').fadeOut(0);

    $('#player').children().filter("video").each(function () {
        this.pause();
        delete (this);
        $(this).remove();
    });

    $('#player').empty();
    $('#player').html(htmlData);
    $('#player').delay(1000).fadeIn(300);

    currentTimeout = window.setTimeout(getNextAsset, (duration * 1000)+1000)
}

//function navigateToNextAsset() {

//    if (currentIndex == 0) {
//        window.location.href = "/home/player";
//    }
//    else {
//        window.location.href = "/home/player?wantedasset=" + currentIndex;
//    }


//}

//function getParameterByName(name, url) {
//    if (!url) url = window.location.href;
//    name = name.replace(/[\[\]]/g, "\\$&");
//    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
//        results = regex.exec(url);
//    if (!results) return null;
//    if (!results[2]) return '';
//    return decodeURIComponent(results[2].replace(/\+/g, " "));
//}
