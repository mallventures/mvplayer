﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace MVPlayer.Migrations
{
    public partial class InitialCreate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "AssetItems",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    Duration = table.Column<int>(nullable: false),
                    StartDate = table.Column<DateTime>(nullable: true),
                    EndDate = table.Column<DateTime>(nullable: true),
                    SortOrder = table.Column<int>(nullable: false),
                    Source = table.Column<string>(nullable: true),
                    MimeType = table.Column<string>(nullable: true),
                    Type = table.Column<int>(nullable: false),
                    Key = table.Column<int>(nullable: false),
                    WeekDays = table.Column<string>(nullable: true),
                    IsLocal = table.Column<bool>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AssetItems", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "AssetItems");
        }
    }
}
