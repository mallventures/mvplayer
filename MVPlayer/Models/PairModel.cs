﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MVPlayer.Models
{
    public class PairRequest
    {
        public string IpAddress { get; set; }
        public string MacAddress { get; set; }
    }
    public class PairResponse
    {
        public string Token { get; set; }
        public string Pin { get; set; }
        public DateTime ValidUntil { get; set; }
    }
}
