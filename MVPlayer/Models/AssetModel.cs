﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MVPlayer.Models
{
    public class Asset
    {
        public int duration { get; set; }
        public DateTime? startDate { get; set; }
        public DateTime? endDate { get; set; }
        public int sortOrder { get; set; }
        public int id { get; set; }
        public string source { get; set; }
        public string mimeType { get; set; }
        public int type { get; set; }
        public string weekDays { get; set; }
    }
}
