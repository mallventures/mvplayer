﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using MVPlayer.Models;

namespace MVPlayer.Models
{
    public class PlayerContext : DbContext
    {
        public PlayerContext(DbContextOptions<PlayerContext> options)
            : base(options)
        { }

        public DbSet<AssetItem> AssetItems { get; set; }


    }

    public class AssetItem
    {
        public int Id { get; set; }
        public int Duration { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public int SortOrder { get; set; }
        public string Source { get; set; }
        public string MimeType { get; set; }
        public int Type { get; set; }
        public int Key { get; set; }
        public string WeekDays { get; set; }
        public bool IsLocal { get; set; }
    }

}
